####
# Secret
####

output "id" {
  value = aws_secretsmanager_secret.this.id
}

output "arn" {
  value = aws_secretsmanager_secret.this.arn
}

####
# Rotation
####

output "rotation_enabled" {
  value = aws_secretsmanager_secret.this.rotation_enabled
}
